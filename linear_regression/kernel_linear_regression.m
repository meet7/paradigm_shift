#// follow steps from simple_linear_regression.m
x = csvread('sample.txt');
m = length(x(:,1))
F = [ones(m,1), x(:,1)]
y = x(:,2);

#{
// get square of each input parameter value
// to improve accuracy
#}
F2 = [F, (F(:,2).^2)]

[p] = LinearRegression(F2,y)
plot(F,y,'+b',F,F2*p,'-g')
