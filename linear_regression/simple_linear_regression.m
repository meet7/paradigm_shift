#{
// For any of the following lines, to see output
// remove semi-colon at the end

// read sample data into matrix x
#}

x = csvread('sample.txt');

#{
// slicing the input matrix
// F -> input parameter vector
// get the first coln of x into F
#}
m = length(x(:,1))
F = [ones(m,1), x(:,1)]

#{
// y -> output paramter vector
// get the second coln of x into y
#}
y = x(:,2);

#// weights are stored in p
[p] = LinearRegression(F,y)

#// plot the graph
plot(F,y,'+b')
plot(F,y,'+b',F,F*p,'-g')
