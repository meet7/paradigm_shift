#// follow steps from simple_linear_regression.m
x = csvread('sample.txt');
m = length(x(:,1))
F = [ones(m,1), x(:,1), x(:,3)]
y = x(:,2);

#{
// get square, cube of each input parameter value
// to improve accuracy
#}
F3 = [F, (F(:,2).^2), (F(:,3).^2), (F(:,2).^3), (F(:,3).^2)];

[p] = LinearRegression(F3,y)
plot(F,y,'+b',F,F3*p,'-g')