class Parameters
{
    public:
    Parameters();
    int get_ink_level();
    int get_num_of_bw_pages();
    int get_num_of_colour_pages();
    int get_ink_per_bw_page();
    int get_ink_per_colour_page();
    int get_est_bw_pages_remaining();
    int get_est_colour_pages_remaining();
    void set_ink_level(int ink_level);
    void set_num_of_bw_pages(int bw_pages);
    void set_num_of_colour_pages(int colour_pages);
    void set_ink_per_bw_page(int ink_per_bw_page);  
    void set_ink_per_colour_page(int ink_per_colour_page);
    void set_est_bw_pages_remaining(int est_pages);
    void set_est_colour_pages_remainig(int est_pages);
    // should be called after every print so that avg keeps improving
    void calc_ink_per_bw_page(int num_of_bw_pages, int current_ink_level);
    void calc_ink_per_colour_page(int num_of_colour_pages, int current_ink_level);

    private:
    int ink_level;
    int num_of_bw_pages;
    int num_of_colour_pages;
    int ink_per_bw_page;
    int ink_per_colour_page;
    int est_bw_pages_remaining;
    int est_colour_pages_remaining;
}