// should be called after every page print
void calc_average(const Parameters& param)
{
    int ink_level = param.get_ink_level();
    int num_of_bw_pages = param.get_num_of_bw_pages();
    int num_of_colour_pages = param.get_num_of_colour_pages();
    int ink_per_bw_page = param.get_ink_per_bw_page();
    int ink_per_colour_page = param.get_ink_per_colour_page();

}