/*********************************************
* filename: json_utility.h
*********************************************
*
* Wrapper methods over lib json methods
*
*********************************************/

#ifndef _JSON_UTILITY_H_
#define _JSON_UTILITY_H_

#include <parameters.h>

extern "C"
{
	#include "sirius_types.h"
	#include "lib_json.h"	
}

namespace json_utility
{
	typedef  JsonNode* JsonNodePtr;
	
	// void create_string_node(JsonNodePtr&, const char* node_name, const char * node_value);
	// void prepend_string_node(JsonNodePtr&, const char* node_name, const char * node_value);
	// void create_integer_node(JsonNodePtr&, const char* node_name, uint32 node_value);
	// void append_json_node(JsonNodePtr&, const char* node_name, JsonNodePtr node_value);
	// void prepend_json_node(JsonNodePtr&, const char* node_name, JsonNodePtr node_value);
	JsonNodePtr create_json_object(const Parameters& param);
	JsonNodePtr create_json_array();
}

#endif /* _JSON_WRAPPER_H_ */
