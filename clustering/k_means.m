#{
  first col: ink_level_c
  second col: ink_level_m
  third_col: ink_level_y
  fourth_col: ink_level_k
  fifth col: num of pages printed
#}

x = csvread('sample1.txt');
m = length(x(:,1))
data = [x(:,1), x(:,2),x(:,3),x(:,4),x(:,5)];
data2 = [data, (data(:,1).^2), (data(:,2).^2), (data(:,3).^2), (data(:,4).^2), (data(:,5).^2)];

# perform clustering
[idx, centers] = kmeans (data2, 2);

## Plot the result
figure;
plot (data (idx==1, 1), data (idx==1, 2), 'ro');
hold on;
plot (data (idx==2, 1), data (idx==2, 2), 'bs');
plot (centers (:, 1), centers (:, 2), 'kv', 'markersize', 10);
hold off;