# paradigm_shift

Place to experiment various ML algos in Octave.

## Installation

```bash
sudo apt-get install octave
sudo apt-get install octave-optim
```

To load optim pckg, run following cmd in octave terminal.
Same can be done to load other required packages

```bash
pkg load optim
```

For any additional pckg required search using

```bash
sudo apt-cache search pckg_name
```


## Octave Help
Refer docs at [Octave](https://octave.sourceforge.io/docs.php)

Or run following cmd in octave terminal

```bash
doc fn_name
```
